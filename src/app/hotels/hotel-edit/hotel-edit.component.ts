import { NumberValidators } from './../shared/validators/numbers.validators';
import { GlobalGenericValidator } from './../shared/validators/global-generic.validator';
import { IHotel } from './../shared/models/hotel';
import { HotelListService } from 'src/app/hotels/shared/services/hotel-list.service';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, fromEvent, merge, Observable, timer } from 'rxjs';
import { debounce } from 'rxjs/operators';

@Component({
  selector: 'app-hotel-edit',
  templateUrl: './hotel-edit.component.html',
  styleUrls: ['./hotel-edit.component.css']
})
export class HotelEditComponent implements OnInit, AfterViewInit {

  @ViewChildren(FormControlName, { read: ElementRef }) inputElements: ElementRef[];

  public hotelForm: FormGroup;

  public hotel: IHotel;

  public pageTitle: string;

  public errorMessage: string;

  public formErrors: { [key: string]: string } = {};

  public validationMessages: { [key: string]: { [key: string]: string } } = {
    hotelName: {
      required: "Le nom de l'hotel est obligatoire",
      minlength: "Le nom de l'hotel doit comporter au moins 4 caractères"
    },
    price: {
      required: "Le prix de l'hotel est obligatoire",
      pattern: "Le prix de l'hotel doit être un nombre"
    },
    rating: {
      range: "Donner une note compris entre 1 et 5"
    }
  }

  private globalGenericValidator: GlobalGenericValidator;

  private isFormSubmitted: Boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private hotelService: HotelListService
  ) { }

  ngOnInit(): void {
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.hotelForm = this.fb.group({
      hotelName: ['', [Validators.required, Validators.minLength(4)]],
      price: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      rating: ['', NumberValidators.range(1, 5)],
      description: [''],
      tags: this.fb.array([])
    })
    this.route.paramMap.subscribe(params => {
      const id = +params.get('id');
      this.getSelectedHotel(id)
    })
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    const formControlBlurs: Observable<unknown>[] = this.inputElements.map((formControlElemRef: ElementRef) => fromEvent(formControlElemRef.nativeElement, 'blur'));
    merge(this.hotelForm.valueChanges, ...formControlBlurs)
      .pipe(debounce(() => this.isFormSubmitted ? EMPTY : timer(800)))
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.hotelForm, this.isFormSubmitted);
        console.log('errors: ', this.formErrors);
      })
  }

  /**
   * hideError
   */
  public hideError() {
    this.errorMessage = null;
  }

  public getSelectedHotel(id: number): void {
    this.hotelService.getHotelById(id).subscribe((hotel: IHotel) => {
      this.displayHotel(hotel);
    })
  }

  public get tags(): FormArray {
    return this.hotelForm.get('tags') as FormArray;
  }

  public addTag(): void {
    this.tags.push(new FormControl());
  }

  public deleteTag(index: number): void {
    this.tags.removeAt(index);
    this.tags.markAsDirty();
  }

  public displayHotel(hotel: IHotel): void {
    this.hotel = hotel;

    if (this.hotel.id === 0) {
      this.pageTitle = "Créer un hotel";
    } else {
      this.pageTitle = `Modifier l'hotel ${this.hotel.hotelName}`
    }

    this.hotelForm.patchValue({
      hotelName: this.hotel.hotelName,
      price: this.hotel.price,
      rating: this.hotel.rating,
      description: this.hotel.description
    });
    this.hotelForm.setControl('tags', this.fb.array(this.hotel.tags || []))
  }

  /**
     * saveHotel
     */
  public saveHotel(): void {
    this.isFormSubmitted = true;
    this.hotelForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });
    if (this.hotelForm.valid) {
      if (this.hotelForm.dirty) {
        const hotel: IHotel = {
          ... this.hotel,
          ... this.hotelForm.value
        }
        if (hotel.id === 0) {
          this.hotelService.createHotel(hotel).subscribe({
            next: () => this.saveCompleted(),
            error: (err) => this.errorMessage = err
          })
        } else {
          this.hotelService.updateHotel(hotel).subscribe({
            next: () => this.saveCompleted(),
            error: (err) => this.errorMessage = err
          })
        }
      }
    } else {
      this.errorMessage = "Corriger les erreurs"
    }
  }

  /**
   * saveCompleted
   */
  public saveCompleted() {
    this.hotelForm.reset();
    this.router.navigate(['/hotels'])
  }

  /**
   * deleteHotel
   */
  public deleteHotel(): void {
    if (confirm(`Voulez-vous supprimer ${this.hotel.hotelName} ?`)) {
      this.hotelService.deleteHotel(this.hotel.id).subscribe({
        next: () => this.saveCompleted()
      })
    }
  }
}
