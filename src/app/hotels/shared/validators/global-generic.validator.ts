import { FormGroup } from "@angular/forms";

export class GlobalGenericValidator {
  constructor(
    private validationMessages: { [key: string]: { [key: string]: string } }
  ) { }

  public createErrorMessage(container: FormGroup, isFormSubmitted?: Boolean): { [key: string]: string } {
    const errorMessages = {};

    for (const controlName in container.controls) {
      if (container.controls.hasOwnProperty(controlName)) {
        const selectedControl = container.controls[controlName];
        if (this.validationMessages[controlName]) {
          errorMessages[controlName] = '';
          if ((selectedControl.dirty || selectedControl.touched || isFormSubmitted) && selectedControl.errors) {
            Object.keys(selectedControl.errors).map((errorMessagesKey: string) => {
              if (this.validationMessages[controlName][errorMessagesKey]) {
                errorMessages[controlName] += this.validationMessages[controlName][errorMessagesKey] + ' ';
              }
            })
          }
        }
      }
    }
    return errorMessages;
  }
}
