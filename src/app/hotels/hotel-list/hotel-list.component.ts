import { Component, OnInit } from "@angular/core";
import { IHotel } from "../shared/models/hotel";
import { HotelListService } from '../shared/services/hotel-list.service'

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
})

export class HotelListComponent implements OnInit {
  //Propriétés

  //title
  public title = 'Liste hotels';

  //liste d'hotels
  public hotels: IHotel[] = [];

  //Badge
  public showBadge: boolean = true;

  //filtre hotel
  private _hotelFilter = 'mot';

  //hotels filtrés
  public filteredHotels: IHotel[] = [];

  //Etoile reçue
  public receivedRating: string;

  //erreur message
  public errorMsg: string;

  constructor(private HotelListService: HotelListService) { }

  //Cycle de vie
  ngOnInit() {
    this.HotelListService.getHotels().subscribe({
      next: hotels => {
        this.hotels = hotels;
        this.filteredHotels = this.hotels;
      },
      error: err => this.errorMsg = err
    });
    this.hotelFilter = '';
  }

  //Méthodes

  /**
   * toggleIsNewBadge
   * return rien
   */
  public toggleIsNewBadge(): void {
    this.showBadge = !this.showBadge;
  }

  /**
   * getter hotelFilter
   */
  public get hotelFilter(): string {
    return this._hotelFilter;
  }

  /**
   * setter hotelFilter
   */
  public set hotelFilter(filter: string) {
    this._hotelFilter = filter;

    /**
     * si on a un critere de recherche "this.hotelFilter",
     * alors on retourne le resultat de la recherche "this.filterHotels(this.hotelFilter)",
     * sinon on retourne la lste complete des hotels
    */
    this.filteredHotels = this.hotelFilter ? this.filterHotels(this.hotelFilter) : this.hotels;
  }

  //Filtre le nom de l'hotel par rapport au critere de recherche
  private filterHotels(criteria: string): IHotel[] {
    criteria = criteria.toLocaleLowerCase();
    const res = this.hotels.filter(
      (hotel: IHotel) => hotel.hotelName.toLocaleLowerCase().indexOf(criteria) != -1
    );
    return res;
  }

  /**
   * receiveRatingClicked
   */
  public receiveRatingClicked(message: string): void {
    this.receivedRating = message;
  }
}
