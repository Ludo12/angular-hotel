import { Pipe, PipeTransform } from "@angular/core";

@Pipe({

  name: 'replaceComma'
})

export class ReplaceComma implements PipeTransform {
  transform(value: string): string {
    //si la valeur n'est ni undefined ni null
    if (!!value) {
      return value.replace(/,/g, '.');
    } else {
      return '';
    }
  }
}
